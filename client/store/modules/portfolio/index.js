import * as typesMutations from './mutations-types'
import * as typesActions   from './actions-types'

const state = () => ({
    portfolio: null
})

const mutations = {
    [typesMutations.UPDATE_PORTFOLIO] (state, object) {
        state.portfolio = object
    }
}

const actions = {
    [typesActions.UPDATE_PORTFOLIO] ({ state, commit }, object) {
        commit(typesMutations.UPDATE_PORTFOLIO, object)
    }
}


const getters = {
    portfolio (state) {
        return state.portfolio
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
