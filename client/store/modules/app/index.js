import * as typesMutations from './mutations-types'
import * as typesActions   from './actions-types'

const state = () => ({
    isLoading: false,
    lang: 'en'
})

const mutations = {
    [typesMutations.UPDATE_IS_LOADING] (state, boolean) {
        state.isLoading = boolean
    },

    [typesMutations.UPDATE_LANG] (state, lang) {
        state.lang = lang
    }
}

const actions = {
    [typesActions.UPDATE_IS_LOADING] ({ state, commit }, boolean) {
        commit(typesMutations.UPDATE_IS_LOADING, boolean)
    },

    [typesActions.UPDATE_LANG] ({ state, commit }, lang) {
        commit(typesMutations.UPDATE_LANG, lang)
    }
}


const getters = {
    isLoading (state) {
        return state.isLoading
    },

    lang (state) {
        return state.lang
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
