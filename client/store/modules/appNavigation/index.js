import * as typesMutations from './mutations-types'
import * as typesActions   from './actions-types'

const state = () => ({
    isOpen: false
})

const mutations = {
    [typesMutations.UPDATE_IS_OPEN] (state, boolean) {
        state.isOpen = boolean
    }
}

const actions = {
    [typesActions.UPDATE_IS_OPEN] ({ state, commit }, boolean) {
        commit(typesMutations.UPDATE_IS_OPEN, boolean)
    }
}


const getters = {
    isOpen (state) {
        return state.isOpen
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}

