import Vue  from 'vue'
import Vuex from 'vuex'

import app            from './modules/app'
import appNavigation  from './modules/appNavigation'
import portfolio      from './modules/portfolio'

Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        app,
        appNavigation,
        portfolio,
    }
})
