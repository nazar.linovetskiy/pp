const express = require('express');
const router = express.Router();

const articles = [{id: "1", title: "Article title #1"},{id: "2", title: "Article title #2"},{id: "3", title: "Article title #3"}];


router.get('/', (req, res, next) => {
    res.status(200).json(articles);
});

// create article
router.post('/', (req, res, next) => {
    const article = {
        title: req.body.title
    };

    res.status(200).json({
        message: "200: Handling POST request to '/articles'.",
        createdArticle: article
    });
});

router.get('/:id', (req, res, next) => {
    const id = req.params.id;

    res.status(200).json({id});
});

router.patch('/:id', (req, res, next) => {
    res.status(200).json({
        message: "200: Updated article by ID!"
    });
});

router.delete('/:id', (req, res, next) => {
    res.status(200).json({
        message: "200: Deleted article by ID!"
    });
});

module.exports = router;
