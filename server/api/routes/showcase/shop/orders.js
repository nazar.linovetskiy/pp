const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../../../models/showcase/shop/order');


router.get('/', (req, res, next) => {
    res.status(200).json(orders);
});

// create order
router.post('/', (req, res, next) => {
    const order = new Order({
        _id: mongoose.Types.ObjectId(),
        quantity: req.body.quantity,
        product: req.body.productId
    });

    order
    .save()
    .than((result) => {
        res.status(201).json(result);
    })
    .catch((error) => {
        res.status(500).json({error});
    });

    res.status(201).json({
        message: "201: Handling POST request to '/orders'.",
        createdOrder: order
    });
});

router.get('/:id', (req, res, next) => {
    const id = req.params.id;

    res.status(200).json({id});
});

router.patch('/:id', (req, res, next) => {
    res.status(200).json({
        message: "200: Updated order by ID!"
    });
});

router.delete('/:id', (req, res, next) => {
    res.status(200).json({
        message: "200: Deleted order by ID!"
    });
});

module.exports = router;
