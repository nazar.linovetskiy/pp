const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Product = require('../../../models/showcase/shop/product');


router.get('/', (req, res, next) => {
    Product
    .find()
    .select("title price _id")
    .exec()
    .then((docs) => {
        const response = {
            count: docs.length,
            products: docs
        };
        res.status(200).json(response);
    })
    .catch((error) => {
        res.status(500).json({error});
    });
});

// create product
router.post('/', (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        price: req.body.price
    });

    product.save()
    .then( (result) => {
        res.status(201).json({
            message: "Created product successfully.",
            createdProduct: {
                title: result.title,
                price: result.price,
                _id: result._id
            }
        });
    })
    .catch( (error) => {
        res.status(500).json({error});
    });
});

router.get('/:id', (req, res, next) => {
    const id = req.params.id;

    Product
    .findById(id)
    .select("title price _id")
    .exec()
    .then((doc) => {
        if (doc) {
            res.status(200).json({
                product: doc
            });
        } else {
            res.status(404).json({message: 'No valid entry found for provided ID.'});
        }
    })
    .catch((error) => {
        res.status(500).json({error});
    });
});

router.patch('/:id', (req, res, next) => {
    const id = req.params.id;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.prop] = ops.value;
    }

    Product.findOneAndUpdate({_id: id}, {$set: updateOps})
    .then((result) => {
        res.status(200).json(result);
    })
    .cath((error) => {
        res.status(500).json({error});
    });
});

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;

    Product.remove({_id: id})
    .exec()
    .then((result) => {
        res.status(200).json(result);
    })
    .catch((error) => {
        res.status(500).json({error});
    });
});

module.exports = router;
