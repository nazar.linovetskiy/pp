const express    = require('express');
const morgan     = require('morgan');
const bodyParser = require('body-parser');
const mongoose   = require('mongoose');
// Rouetes
const articlesRoutes     = require('./api/routes/blog/articles');
const shopProductsRoutes = require('./api/routes/showcase/shop/products');
const shopOrdersRoutes   = require('./api/routes/showcase/shop/orders');

// Connect to DB
mongoose.connect('mongodb://liamlime:9334500cvbcvb@cluster0-shard-00-00-cbqee.mongodb.net:27017,cluster0-shard-00-01-cbqee.mongodb.net:27017,cluster0-shard-00-02-cbqee.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', {
    useNewUrlParser: true
});
mongoose.Promise = global.Promise;
// Create Express App
const app = express();

// middlewere метод. Срабатывает при каждом запросе
// Use срабатывают в порядке их объявления
// Если не указан первый параметр (Route), то срабатывает для всех запросов
// Всем передается в качестве параметра: req, res, next
// next - отправляет к следующему use (middleware)

// Use console request logging
app.use( morgan('dev') );
// Use Bodyparser
app.use( bodyParser.urlencoded({extended: false}) );
// Use Bodyparser
app.use( bodyParser.json() );

// CORS Headers settings
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, PATCH');
        res.status(200).json({});
    }
    next();
});


// Routes
app.use('/blog/articles', articlesRoutes);
app.use('/showcase/shop/products', shopProductsRoutes);
app.use('/showcase/shop/orders', shopOrdersRoutes);


// Errors Handling
app.use((req, res, next) => {
    const error = new Error('Not Found.');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
